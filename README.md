# Joseph Matthias Goh's Resume

## Download

The latest updated PDF version can be found at [https://gitlab.com/joeir/el-curriculum/blob/master/resume-joesph-matthias-goh.pdf](https://gitlab.com/joeir/el-curriculum/blob/master/resume-joesph-matthias-goh.pdf)

To download it, use [THIS LINK INSTEAD](http://bit.ly/joeircv2020)

## Setting Up

### Ubuntu 20.04

```sh
sudo apt install texlive chktex texlive-latex-base texlive-latex-recommended texlive-latex-extra texlive-extra-utils latexmk;
```

### Ubuntu 16.04

```sh
sudo apt-get install texlive texlive-latex-base latexmk;
```

## Contributing

Use Visual Studio code and install the [LaTeX Workshop extension by James Yu](https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop). Enable the PDF preview mode and you should be able to view updates live.

# License

Seriously?

# 🥂
